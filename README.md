**Trello** link: [OPEN](https://trello.com/invite/b/AEJZu83X/c29e6bd6660c4246a3f3609f33aa74f6/console-application-work-item-management)

**Project Description**

Work Item Management (WIM) Console Application. 

**Functionality**

Application supports multiple teams. Each team has name, members and boards. 

Member has name, list of work items and activity history. 

Board has name, list of work items and activity history. 

There are 3 types of work items: bug, story and feedback. 

**Bug**

Bug has ID, title, description, steps to reproduce, priority, severity, status, assignee, comments and history. 

**Story** 

Story has ID, title, description, priority, size, status, assignee, comments and history. 
 
**Feedback** 

Feedback has ID, title, description, rating, status, comments and history. 

**Operations** 

Application supports the following operations: 
* Create a new person 
* Show all people 
* Show person's activity 
* Create a new team 
* Show all teams 
* Show team's activity 
* Add person to team 
* Show all team members 
* Create a new board in a team 
* Show all team boards 
* Show board's activity 
* Create a new Bug/Story/Feedback in a board 
* Change Priority/Severity/Status of a bug 
* Change Priority/Size/Status of a story 
* Change Rating/Status of a feedback 
* Assign/Unassign work item to a person 
* Add comment to a work item 
* List work items with options: 
**List all Filter bugs/stories/feedback only* 
**Filter by status and/or asignee*
**Sort by title/priority/severity/size/rating*


