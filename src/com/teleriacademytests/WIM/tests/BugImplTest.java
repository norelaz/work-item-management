package com.teleriacademytests.WIM.tests;

import com.telerikacademy.workitemmanagement.contracts.Bug;
import com.telerikacademy.workitemmanagement.enums.PriorityType;
import com.telerikacademy.workitemmanagement.enums.SeverityType;
import com.telerikacademy.workitemmanagement.enums.StatusType;
import com.telerikacademy.workitemmanagement.models.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BugImplTest {
    private Bug bug;

    @Before
    public void initBug() {
        bug = new BugImpl("123", "TestTitle10", "TestDescription", StatusType.ACTIVE, PriorityType.LOW, SeverityType.MINOR);
    }

    @Test
    public void getId() {
        Assert.assertEquals("Get ID method doesn't work", "123", bug.getId());
    }

    @Test
    public void setId() {
        Assert.assertEquals("Set ID method doesn't work", "123", bug.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getTitle() {
        //Arrange, Act
        Bug bugWrongTitle = new BugImpl("123", "TestTitle", "TestDescription", StatusType.ACTIVE, PriorityType.LOW, SeverityType.MINOR);
        //Act
        bugWrongTitle.getTitle();
    }

    @Test
    public void getDescription() {
        //Assert
        Assert.assertEquals("Get description method doesn't work", "TestDescription", bug.getDescription());
    }

    @Test
    public void addComment() {
        //Arrange
        bug.addComment("test comment 1");
        bug.addComment("test comment 2");
        //Assert
        Assert.assertEquals("Method add comment to bug doesn't work", 2, bug.getComments().size());
    }

    @Test
    public void getPriorityType() {
        //Assert
        Assert.assertEquals("Method get priority type doesn't work", PriorityType.LOW, bug.getPriorityType());
    }

    @Test
    public void validPriorityType() {
        //Arrange, Act
        bug = new BugImpl("123", "TestTitle10", "TestDescription",
                StatusType.ACTIVE, PriorityType.HIGH, SeverityType.MINOR);
        //Assert
        Assert.assertEquals("Method set priority type doesn't work", PriorityType.HIGH, bug.getPriorityType());
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenInvalidPriorityType() {
        //Act
        bug.setPriorityType(PriorityType.valueOf("Invalid type"));
    }

    @Test
    public void getSeverityType() {
        //Assert
        Assert.assertEquals("Method get severity type doesn't work", SeverityType.MINOR, bug.getSeverityType());
    }

    @Test
    public void validSeverityType() {
        //Arrange, act
        bug = new BugImpl("123", "TestTitle10", "TestDescription",
                StatusType.ACTIVE, PriorityType.HIGH, SeverityType.CRITICAL);
        //Assert
        Assert.assertEquals("Method set priority type doesn't work", SeverityType.CRITICAL, bug.getSeverityType());
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenInvalidSeverityType() {
        //Arrange, Act
        bug = new BugImpl("123", "TestTitle10", "TestDescription",
                StatusType.ACTIVE, PriorityType.HIGH, SeverityType.valueOf("Invalid type"));
    }

    @Test
    public void getStatusType() {
        //Assert
        Assert.assertEquals("Get status type method doesn't work", StatusType.ACTIVE, bug.getStatusType());
    }

    @Test
    public void validStatusType() {
        //Assert
        Assert.assertEquals(StatusType.ACTIVE, bug.getStatusType());
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenInvalidBugStatusType() {
        // Act
        bug.setStatusType(StatusType.INPROGRESS);
    }
}