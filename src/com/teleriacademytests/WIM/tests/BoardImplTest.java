package com.teleriacademytests.WIM.tests;

import com.telerikacademy.workitemmanagement.contracts.Board;
import com.telerikacademy.workitemmanagement.enums.PriorityType;
import com.telerikacademy.workitemmanagement.enums.SeverityType;
import com.telerikacademy.workitemmanagement.enums.SizeType;
import com.telerikacademy.workitemmanagement.enums.StatusType;
import com.telerikacademy.workitemmanagement.models.BoardImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BoardImplTest {
    private Board board;

    @Before
    public void initBoard() {
        board = new BoardImpl("TestBoard");
    }

    @Test
    public void createNewBug() {
        //Arrange, Act, Assert
        board.createNewBug("17", "BugTitle12", "testBugDescription",
                StatusType.ACTIVE, PriorityType.LOW, SeverityType.MAJOR);
        //@TODO check is this necessary
        Assert.assertEquals("Method create new Bug doesn't work", 1, board.getWorkItems().size());
    }

    @Test
    public void createNewStory() {
        //Arrange, Act, Assert
        board.createNewStory("4", "StoryTitle1234", "testStoryDescription",
                StatusType.NOTDONE, PriorityType.LOW, SizeType.LARGE);
        Assert.assertEquals("Method create new Story doesn't work", 1, board.getWorkItems().size());
    }

    @Test
    public void createNewFeedback() {
        //Arrange, Act, Assert
        board.createNewFeedback("4321", "FeedbackTitle", "testFeedbackDescription",
                StatusType.DONE, 12);
        Assert.assertEquals("Method create new Feedback doesn't work", 1, board.getWorkItems().size());
    }
}