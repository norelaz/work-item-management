package com.teleriacademytests.WIM.tests;

import com.telerikacademy.workitemmanagement.contracts.Feedback;
import com.telerikacademy.workitemmanagement.enums.StatusType;
import com.telerikacademy.workitemmanagement.models.FeedbackImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FeedbackImplTest {

    private Feedback feedback;

    @Before
    public void initFeedback() {
        feedback = new FeedbackImpl("4321", "FeedbackTitle", "testFeedbackDescription", StatusType.DONE, 12);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenInvalidRating() {
        int negative = -4;
        //Act, assert
        feedback.setRating(negative);
    }

    @Test
    public void setValidStatusType() {
        //Act, Assert
        feedback.setStatusType(StatusType.DONE);
        Assert.assertEquals(StatusType.DONE, feedback.getStatusType());
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenInvalidBugStatusType() {
        // Act
        feedback.setStatusType(StatusType.INPROGRESS);
    }

    @Test
    public void addComment() {
        //Arrange
        feedback.addComment("test comment 1");
        feedback.addComment("test comment 2");
        //Assert
        Assert.assertEquals("Method add comment to bug doesn't work", 2, feedback.getComments().size());
    }
}