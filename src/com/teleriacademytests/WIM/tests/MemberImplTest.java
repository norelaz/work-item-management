package com.teleriacademytests.WIM.tests;

import com.telerikacademy.workitemmanagement.contracts.Assignable;
import com.telerikacademy.workitemmanagement.contracts.Board;
import com.telerikacademy.workitemmanagement.contracts.Member;
import com.telerikacademy.workitemmanagement.enums.PriorityType;
import com.telerikacademy.workitemmanagement.enums.SeverityType;
import com.telerikacademy.workitemmanagement.enums.SizeType;
import com.telerikacademy.workitemmanagement.enums.StatusType;
import com.telerikacademy.workitemmanagement.models.BoardImpl;
import com.telerikacademy.workitemmanagement.models.BugImpl;
import com.telerikacademy.workitemmanagement.models.MemberImpl;
import com.telerikacademy.workitemmanagement.models.StoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MemberImplTest {

    private Member member;
    private Board board;
    @Before
    public void initBoardAndMember() {
        board = new BoardImpl("TestBoard");
        member = new MemberImpl("TestMember");
    }


    @Test(expected = IllegalArgumentException.class)
    public void throwWhenTheNameIsSmallerThanMinValue() {
        member = new MemberImpl("Test");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenTheNameIsLargerThanMaxValue() {
        member = new MemberImpl("1234567890123456");
    }

    @Test
    public void createMemberWithCorrectName() {
        member = new MemberImpl("Pesho");
    }

    @Test
    public void getWorkItems() {
        board = new BoardImpl("TestBoard");
        board.createNewBug("123", "TestTitle10", "TestDescription", StatusType.ACTIVE, PriorityType.LOW, SeverityType.MINOR);
        //Assert
        Assert.assertEquals("Method get work items doesn't work", 1 , board.getWorkItems().size());
    }

    @Test
    public void getHistory() {
        member = new MemberImpl("Pesho");

        Assert.assertEquals("Method get work items doesn't work",
                "Member Pesho history:\n" + " -Member created",
                member.getHistory());
    }

    @Test
    public void assignWorkItem() {
        //Arrange
        Assignable bug = new BugImpl("123", "TestTitle10", "TestDescription", StatusType.ACTIVE, PriorityType.LOW, SeverityType.MINOR);
        member = new MemberImpl("Pesho");

        //Act
        member.assignWorkItem(bug);

        //Assert
        Assert.assertEquals("Method assign work item doesn't work", 1 , member.getWorkItems().size());
    }

    @Test
    public void unAssignWorkItem() {
        //Arrange
        Assignable bug = new BugImpl("123", "TestTitle10", "TestDescription", StatusType.ACTIVE, PriorityType.LOW, SeverityType.MINOR);
        Assignable story = new StoryImpl("4", "StoryTitle1234", "testStoryDescription",
                StatusType.NOTDONE, PriorityType.LOW, SizeType.LARGE);
        member = new MemberImpl("Pesho");

        //Act
        member.assignWorkItem(bug);
        member.assignWorkItem(story);
        member.unassignWorkItem(bug);
//      Assert
        Assert.assertEquals("Method unAssign work item doesn't work", 1 , member.getWorkItems().size());
    }
}