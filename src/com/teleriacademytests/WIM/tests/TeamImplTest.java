package com.teleriacademytests.WIM.tests;

import com.telerikacademy.workitemmanagement.contracts.Member;
import com.telerikacademy.workitemmanagement.contracts.Team;
import com.telerikacademy.workitemmanagement.models.MemberImpl;
import com.telerikacademy.workitemmanagement.models.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TeamImplTest {
    private Team team;
    private Member member;

    @Before
    public void init() {
        team = new TeamImpl("TestTeam");
        member = new MemberImpl("TestMember");
    }

    @Test
    public void addMemberToTeam() {
        team.addMember(member);
        Assert.assertEquals("Method add member to team doesn't work",
                "Team TestTeam members:\n" + " #TestMember\n",
                team.showTeamMembers());
    }

    @Test
    public void createNewBoardToTeam() {
        team.createNewBoard("ConsoleApp");
        Assert.assertEquals("Method create new board doesn't work",
                "ConsoleApp",
                team.showTeamBoards());
    }
}