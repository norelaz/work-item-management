package com.teleriacademytests.WIM.tests;

import com.telerikacademy.workitemmanagement.contracts.Story;
import com.telerikacademy.workitemmanagement.enums.PriorityType;
import com.telerikacademy.workitemmanagement.enums.SizeType;
import com.telerikacademy.workitemmanagement.enums.StatusType;
import com.telerikacademy.workitemmanagement.models.StoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StoryImplTest {
    private Story story;

    @Before
    public void initStory() {
        story = new StoryImpl("4", "StoryTitle1234", "testStoryDescription", StatusType.NOTDONE, PriorityType.LOW, SizeType.LARGE);
    }

    @Test
    public void validPriorityType() {
        //Assert
        Assert.assertEquals("Method set priority type doesn't work", PriorityType.LOW, story.getPriorityType());
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenInvalidPriorityType() {
        //Act
        story.setPriorityType(PriorityType.valueOf("Invalid type"));
    }

    @Test
    public void validSizeType() {
        //Assert
        Assert.assertEquals("Method valid size type doesn't work",SizeType.LARGE ,story.getSize());
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenInvalidSizeType() {
        //Act
        story.setSize(SizeType.valueOf("Invalid size type"));
    }

    @Test
    public void validStatusType() {
        //Assert
        Assert.assertEquals(StatusType.NOTDONE, story.getStatusType());
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenInvalidBugStatusType() {
        // Act
        story.setStatusType(StatusType.ACTIVE);
    }

    @Test
    public void addComment() {
        //Arrange
        story.addComment("test comment 1");
        story.addComment("test comment 2");
        //Assert
        Assert.assertEquals("Method add comment to story doesn't work", 2, story.getComments().size());
    }
}