package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.enums.StatusType;
import com.telerikacademy.workitemmanagement.contracts.Feedback;

import java.util.ArrayList;
import java.util.List;

public class FeedbackImpl extends WorkItemAbstract implements Feedback {

    private int rating;

    public FeedbackImpl(String id, String title, String description, StatusType statusType, int rating) {
        super(id, title, description, statusType);
        setRating(rating);
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public void setRating(int rating) {
        ratingValidation(rating);
        this.rating = rating;
        addHistory(String.format(Constants.RATING_SET_MESSAGE, getRating()));
    }

    @Override
    protected List<StatusType> getStatusPossibleTypes() {
        List<StatusType> list = new ArrayList<>();
        list.add(StatusType.NEW);
        list.add(StatusType.SCHEDULED);
        list.add(StatusType.UNSCHEDULED);
        list.add(StatusType.DONE);
        return list;
    }

    @Override
    protected String additionalInfo() {
        return String.format(Constants.RATING_MESSAGE, getRating());
    }

    private void ratingValidation(int rating) {
        if (rating < Constants.ZERO) {
            throw new IllegalArgumentException(Constants.RATING_CANNOT_BE_LESS_THAN_ZERO_MESSAGE);
        }
    }
}
