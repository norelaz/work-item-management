package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.contracts.*;
import com.telerikacademy.workitemmanagement.enums.PriorityType;
import com.telerikacademy.workitemmanagement.enums.SeverityType;
import com.telerikacademy.workitemmanagement.enums.SizeType;
import com.telerikacademy.workitemmanagement.enums.StatusType;

public class BoardImpl extends MemberAndBoardAbstract implements Board {

    public BoardImpl(String name) {
        super(name);
    }

    @Override
    public Bug createNewBug(String id, String title, String description, StatusType statusType,
                             PriorityType priorityType, SeverityType severityType) {
        Bug bug = new BugImpl(id, title, description, statusType, priorityType, severityType);
        getOriginalWorkitems().add(bug);
        addActivity(String.format(Constants.ADDING_WORK_ITEM_TO_BOARD_MESSAGE, bug.getTitle()));

        return bug;
    }

    @Override
    public Story createNewStory(String id, String title, String description, StatusType statusType,
                               PriorityType priorityType, SizeType size) {
        Story story = new StoryImpl(id, title, description, statusType, priorityType, size);
        getOriginalWorkitems().add(story);
        addActivity(String.format(Constants.ADDING_WORK_ITEM_TO_BOARD_MESSAGE, story.getTitle()));

        return story;
    }

    @Override
    public Feedback createNewFeedback(String id, String title, String description, StatusType statusType, int rating) {
        Feedback feedback = new FeedbackImpl(id, title, description, statusType, rating);
        getOriginalWorkitems().add(feedback);
        addActivity(String.format(Constants.ADDING_WORK_ITEM_TO_BOARD_MESSAGE, feedback.getTitle()));

        return feedback;
    }

    @Override
    protected String memberOrBoardCreation(){
        return "Board";
    }

    @Override
    protected int getMaxLength() {
        return Constants.BOARD_NAME_MAX_LENGTH;
    }
}
