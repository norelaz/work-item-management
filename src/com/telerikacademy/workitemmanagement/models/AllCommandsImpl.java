package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.contracts.*;
import com.telerikacademy.workitemmanagement.enums.PriorityType;
import com.telerikacademy.workitemmanagement.enums.SeverityType;
import com.telerikacademy.workitemmanagement.enums.SizeType;
import com.telerikacademy.workitemmanagement.enums.StatusType;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AllCommandsImpl implements AllCommands {
    private final EngineImpl engine;
    private final Factory factory;

    public AllCommandsImpl() {
        factory = new FactoryImpl();
        engine = new EngineImpl();
    }

    public String createMember(String memberName) {
        mapHasKey(engine.members, memberName, "yes", String.format(Constants.S_EXISTS, "Member", memberName));

        Member member = factory.createMember(memberName);
        engine.members.put(memberName, member);

        return String.format(Constants.S_CREATED, "Member", memberName);
    }

    public String showAllPeople() {
        mapIsEmpty(engine.members, String.format(Constants.NO_YET, "people"));

        StringBuilder allPeople = new StringBuilder(String.format(Constants.ALL_S_LIST, "people"));
        engine.members.forEach((memberName, member) -> allPeople.append(String.format(" #%s\n", member.toString())));

        return allPeople.toString().trim();
    }

    public String showMemberActivity(String memberName) {
        mapHasKey(engine.members, memberName, "no", String.format(Constants.S_DOESNT_EXIST, "Member", memberName));

        Member member = engine.members.get(memberName);

        return member.getHistory();
    }

    //Finish common validations!!!!
    public String createTeam(String teamName) {
        mapHasKey(engine.teams, teamName, "yes", String.format(Constants.S_EXISTS, "Team", teamName));

        Team team = factory.createTeam(teamName);
        engine.teams.put(teamName, team);

        return String.format(Constants.S_CREATED, "Team", teamName);
    }

    public String showAllTeams() {
        mapIsEmpty(engine.teams, String.format(Constants.NO_YET, "teams"));

        StringBuilder allTeams = new StringBuilder(String.format(Constants.ALL_S_LIST, "teams"));

        engine.teams.forEach((teamName, team) -> allTeams.append(String.format(" #%s\n", teamName)));

        return allTeams.toString().trim();
    }

    public String showTeamActivity(String teamName) {
        mapHasKey(engine.teams, teamName, "no", String.format(Constants.S_DOESNT_EXIST, "Team", teamName));

        StringBuilder teamActivityHistory = new StringBuilder(String.format(Constants.TEAM_S_ACTIVITY, teamName));

        engine.teams.get(teamName).getMembers().forEach((memberName, member) -> teamActivityHistory.append(member.getHistory()));
        engine.teams.get(teamName).getBoards().forEach((boardName, board) -> teamActivityHistory.append(board.getHistory()));

        return teamActivityHistory.toString();
    }

    public String addMemberToTeam(String memberName, String teamName) {
        mapHasKey(engine.teams, teamName, "no", String.format(Constants.S_DOESNT_EXIST, "Team", teamName));
        mapHasKey(engine.members, memberName, "no", String.format(Constants.S_DOESNT_EXIST, "Member", memberName));

        engine.teams.get(teamName).addMember(engine.members.get(memberName));

        return String.format(Constants.MEMBER_S_ADDED_TO_TEAM_S, memberName, teamName);
    }

    public String showAllTeamMembers(String teamName) {
        mapHasKey(engine.teams, teamName, "no", String.format(Constants.S_DOESNT_EXIST, "Team", teamName));

        return engine.teams.get(teamName).showTeamMembers().trim();
    }

    public String createABoardInTeam(String boardName, String teamName) {
        mapHasKey(engine.teams, teamName, "no", String.format(Constants.S_DOESNT_EXIST, "Team", teamName));

        Board board = engine.teams.get(teamName).createNewBoard(boardName);
        engine.boards.put(boardName, board);

        return String.format(Constants.BOARD_S_CREATED_IN_TEAM_S, boardName, teamName);
    }

    public String showAllTeamBoards(String teamName) {
        mapHasKey(engine.teams, teamName, "no", String.format(Constants.S_DOESNT_EXIST, "Team", teamName));

        StringBuilder allTeamBoards = new StringBuilder(String.format(Constants.TEAM_S_BOARDS, teamName));
        engine.teams.get(teamName).getBoards().forEach((boardName, board) -> allTeamBoards.append(String.format(" #%s\n", board.toString())));

        return allTeamBoards.toString().trim();
    }

    public String showBoardActivity(String boardName) {
        mapHasKey(engine.boards, boardName, "no", String.format(Constants.S_DOESNT_EXIST, "Board", boardName));

        return engine.boards.get(boardName).getHistory();
    }

    public String createNewBugInBoard(String boardName, String id, String title, String description, StatusType statusType, PriorityType priorityType, SeverityType severityType) {
        mapHasKey(engine.boards, boardName, "no", String.format(Constants.S_DOESNT_EXIST, "Board", boardName));
        mapHasKey(engine.workItems, id, "yes", String.format(Constants.S_EXISTS, "Workitem", id));

        Bug bug = engine.boards.get(boardName).createNewBug(id, title, description, statusType, priorityType, severityType);
        engine.workItems.put(id, bug);

        return String.format(Constants.S_CREATED_IN_BOARD_S, "Bug", title, boardName);
    }

    public String createNewStoryInBoard(String boardName, String id, String title, String description, StatusType statusType, PriorityType priorityType, SizeType sizeType) {
        mapHasKey(engine.boards, boardName, "no", String.format(Constants.S_DOESNT_EXIST, "Board", boardName));
        mapHasKey(engine.workItems, id, "yes", String.format(Constants.S_EXISTS, "Workitem", id));

        Story story = engine.boards.get(boardName).createNewStory(id, title, description, statusType, priorityType, sizeType);
        engine.workItems.put(id, story);

        return String.format(Constants.S_CREATED_IN_BOARD_S, "Story", title, boardName);
    }

    public String createNewFeedBackInBoard(String boardName, String id, String title, String description, StatusType statusType, int rating) {
        mapHasKey(engine.boards, boardName, "no", String.format(Constants.S_DOESNT_EXIST, "Board", boardName));
        mapHasKey(engine.workItems, id, "yes", String.format(Constants.S_EXISTS, "Workitem", id));

        Feedback feedback = engine.boards.get(boardName).createNewFeedback(id, title, description, statusType, rating);
        engine.workItems.put(id, feedback);

        return String.format(Constants.S_CREATED_IN_BOARD_S, "Feedback", title, boardName);
    }

    public String changeWorkItemfeature(String id, String feature) {
        mapHasKey(engine.workItems, id, "no", String.format(Constants.S_DOESNT_EXIST, "Workitem", id));

        try {
            PriorityType priorityType = PriorityType.valueOf(feature);
            if (engine.workItems.get(id) instanceof Bug) {
                Bug bug = (BugImpl) engine.workItems.get(id);
                bug.setPriorityType(priorityType);
                engine.workItems.put(id, bug);
                return String.format(Constants.CHANGED, "Bug", bug.getTitle(), "PriorityType", priorityType.toString());
            } else if (engine.workItems.get(id) instanceof Story) {
                Story story = (StoryImpl) engine.workItems.get(id);
                story.setPriorityType(priorityType);
                engine.workItems.put(id, story);
                return String.format(Constants.CHANGED, "Story", story.getTitle(), "PriorityType", priorityType.toString());
            } else {
                return String.format(Constants.WORK_ITEM_DOES_NOT_HAVE, "PriorityType");
            }
        } catch (Exception ex) {
            try {
                StatusType statusType = StatusType.valueOf(feature);
                if (engine.workItems.get(id) instanceof Bug) {
                    Bug bug = (BugImpl) engine.workItems.get(id);
                    bug.setStatusType(statusType);
                    engine.workItems.put(id, bug);
                    return String.format(Constants.CHANGED, "Bug", bug.getTitle(), "StatusType", statusType.toString());
                } else if (engine.workItems.get(id) instanceof Story) {
                    Story story = (StoryImpl) engine.workItems.get(id);
                    story.setStatusType(statusType);
                    engine.workItems.put(id, story);
                    return String.format(Constants.CHANGED, "Story", story.getTitle(), "StatusType", statusType.toString());
                } else if (engine.workItems.get(id) instanceof Feedback) {
                    Feedback feedback = (FeedbackImpl) engine.workItems.get(id);
                    feedback.setStatusType(statusType);
                    engine.workItems.put(id, feedback);
                    return String.format(Constants.CHANGED, "Feedback", feedback.getTitle(), "StatusType", statusType.toString());
                }
            } catch (Exception exc) {
                try {
                    SizeType sizeType = SizeType.valueOf(feature);
                    if (engine.workItems.get(id) instanceof Story) {
                        Story story = (StoryImpl) engine.workItems.get(id);
                        story.setSize(sizeType);
                        engine.workItems.put(id, story);
                        return String.format(Constants.CHANGED, "Story", story.getTitle(), "SizeType", sizeType.toString());
                    } else {
                        return String.format(Constants.WORK_ITEM_DOES_NOT_HAVE, "SizeType");
                    }
                } catch (Exception exception) {
                    try {
                        int rating = Integer.parseInt(feature);
                        if (engine.workItems.get(id) instanceof Feedback) {
                            Feedback feedback = (FeedbackImpl) engine.workItems.get(id);
                            feedback.setRating(rating);
                            engine.workItems.put(id, feedback);
                            return String.format(Constants.CHANGED, "Feedback", feedback.getTitle(), "Rating", rating);
                        } else {
                            return String.format(Constants.WORK_ITEM_DOES_NOT_HAVE, "Rating");
                        }
                    } catch (Exception exce) {
                        try {
                            SeverityType severityType = SeverityType.valueOf(feature);
                            if (engine.workItems.get(id) instanceof Bug) {
                                Bug bug = (BugImpl) engine.workItems.get(id);
                                bug.setSeverityType(severityType);
                                engine.workItems.put(id, bug);
                                return String.format(Constants.CHANGED, "Bug", bug.getTitle(), "SeverityType", severityType.toString());
                            } else {
                                return String.format(Constants.WORK_ITEM_DOES_NOT_HAVE, "SeverityType");
                            }
                        } catch (Exception e) {
                            return String.format(Constants.INVALID_ARGUMENT_S, feature);
                        }
                    }
                }
            }
        }
        return "";
    }

    public String assignWorkItemToMember(String memberName, String id) {
        mapHasKey(engine.members, memberName, "no", String.format(Constants.S_DOESNT_EXIST, "Member", memberName));
        mapHasKey(engine.workItems, id, "no", String.format(Constants.S_DOESNT_EXIST, "Workitem", id));

        try {
            engine.members.get(memberName).assignWorkItem((Assignable) engine.workItems.get(id));
        } catch (Exception e) {
            return Constants.FEEDBACK_TO_A_MEMBER_ASSIGN_ERROR_MESSAGE;
        }
        return String.format(Constants.WORK_ITEM_ASSIGNED_TO_S, id, memberName);
    }

    public String unassignWorkItemToMember(String memberName, String id) {
        mapHasKey(engine.members, memberName, "no", String.format(Constants.S_DOESNT_EXIST, "Member", memberName));
        mapHasKey(engine.workItems, id, "no", String.format(Constants.S_DOESNT_EXIST, "Workitem", id));

        try {
            engine.members.get(memberName).unassignWorkItem((Assignable) engine.workItems.get(id));
        } catch (Exception e) {
            return Constants.FEEDBACK_TO_A_MEMBER_UNASSIGN_ERROR_MESSAGE;
        }

        return String.format(Constants.WORK_ITEM_UNASSIGNED_S, id, memberName);
    }

    public String addCommentToAWorkItem(String id, String comment) {
        mapHasKey(engine.workItems, id, "no", String.format(Constants.S_DOESNT_EXIST, "Workitem", id));

        engine.workItems.get(id).addComment(comment);

        return String.format(Constants.COMMENT_ADDED_TO_WORKITEM, id, comment);
    }

    public String listAll() {
        mapIsEmpty(engine.workItems, String.format(Constants.NO_WORK_ITEMS_FOUND));

        StringBuilder output = new StringBuilder(String.format(Constants.ALL_S_LIST, "Workitems"));
        engine.workItems.forEach((id, workItem) -> output.append(String.format(" #%s\n", workItem.toString())));
        return output.toString().trim();
    }

    public String filterByInstance(String filteringArgument) {
        mapIsEmpty(engine.workItems, String.format(Constants.NO_WORK_ITEMS_FOUND));

        List<WorkItem> workItemsList = new ArrayList<>();
        engine.workItems.forEach((id, workItem) -> workItemsList.add(workItem));

        StringBuilder output = new StringBuilder();
        switch (filteringArgument) {
            case "Bugs":
                output.append("All bugs:\n");
                workItemsList.stream().filter(w -> w instanceof BugImpl).forEach(w -> output.append(String.format(" #%s\n", w.toString())));
                emptyStringBuilderValidation(output, String.format(Constants.NO_S_IN_WORKITEMS, "Bugs"));
                return output.toString().trim();
            case "Stories":
                output.append("All stories:\n");
                workItemsList.stream().filter(w -> w instanceof StoryImpl).forEach(w -> output.append(String.format(" #%s\n", w.toString())));
                emptyStringBuilderValidation(output, String.format(Constants.NO_S_IN_WORKITEMS, "Stories"));
                return output.toString().trim();
            case "Feedbacks":
                output.append("All feedbacks:\n");
                workItemsList.stream().filter(w -> w instanceof FeedbackImpl).forEach(w -> output.append(String.format(" #%s\n", w.toString())));
                emptyStringBuilderValidation(output, String.format(Constants.NO_S_IN_WORKITEMS, "Feedbacks"));
                return output.toString().trim();
            default:
                return String.format(Constants.ILLEGAL, "Workitem");
        }
    }

    public String filterByStatus(String type) {
        mapIsEmpty(engine.workItems, String.format(Constants.NO_WORK_ITEMS_FOUND));

        try {
            StatusType statusType = StatusType.valueOf(type);

            StringBuilder output = new StringBuilder();
            List<WorkItem> workItemsList = new ArrayList<>();
            engine.workItems.forEach((id, workItem) -> workItemsList.add(workItem));

            workItemsList.stream().filter(w -> w.getStatusType().equals(statusType)).
                    forEach(w -> output.append(String.format(" #%s\n", w.toString())));
            try {
                emptyStringBuilderValidation(output, String.format(Constants.NO_S_IN_WORKITEMS, statusType.toString()));
            } catch (Exception e) {
                return e.getMessage().trim();
            }
            output.insert(0, String.format(Constants.ALL_WORK_ITEMS_WITH_STATUS_TYPE_S, statusType.toString()));
            return output.toString().trim();
        } catch (Exception ex) {
            return String.format(Constants.ILLEGAL, String.format("StatusType.%s!", type)).trim();
        }
    }

    public String filterByAssignee(String assignee) {
        mapIsEmpty(engine.workItems, String.format(Constants.NO_WORK_ITEMS_FOUND));
        mapHasKey(engine.members, assignee, "no", String.format(Constants.S_DOESNT_EXIST, "Member", assignee));

        StringBuilder output = new StringBuilder();

        engine.members.get(assignee).getWorkItems().forEach(w -> output.append(String.format(" #%s\n", w.toString())));
        emptyStringBuilderValidation(output, String.format(Constants.NO_WORK_ITEMS_ASSIGNED, assignee));
        output.insert(0, String.format(Constants.MEMBER_S_WORK_ITEMS, assignee));
        return output.toString().trim();
    }

    public String filterByStatusAndAssignee(String assignee, String type) {
        mapIsEmpty(engine.workItems, String.format(Constants.NO_WORK_ITEMS_FOUND));
        mapHasKey(engine.members, assignee, "no", String.format(Constants.S_DOESNT_EXIST, "Member", assignee));

        try {
            StatusType statusType = StatusType.valueOf(type);

            StringBuilder output = new StringBuilder();
            //See if exception message can be better!
            try {
                engine.members.get(assignee).getWorkItems().stream().filter(w -> w.getStatusType().equals(statusType)).
                        forEach(w -> output.append(String.format(" #%s\n", w.toString())));
            } catch (Exception e) {
                return e.getMessage().trim();
            }
            try {
                emptyStringBuilderValidation(output, String.format(Constants.S_HAS_NO_WORK_ITEMS_ASSIGNED_WITH_STATUS_TYPE_S, assignee, statusType.toString()));
            } catch (Exception e) {
                return e.getMessage().trim();
            }
            output.insert(0, String.format(Constants.MEMBER_S_WORK_ITEMS_WITH_STATUS_TYPE_S, assignee, statusType));
            return output.toString().trim();
        } catch (Exception ex) {
            return String.format(Constants.ILLEGAL, "StatusType.%s!", type).trim();
        }
    }

    public String listSortedBy(String sortingOption) {
        mapIsEmpty(engine.workItems, String.format(Constants.NO_WORK_ITEMS_FOUND));

        switch (sortingOption) {
            case "Title":
                List<WorkItem> workItemsList = new ArrayList<>();
                engine.workItems.forEach((id, workitem) -> workItemsList.add(workitem));
                workItemsList.sort(Comparator.comparing(WorkItem::getTitle));

                StringBuilder output = new StringBuilder(String.format(Constants.WORK_ITEMS_SORTED_BY_S, "Title"));
                workItemsList.forEach((w) -> output.append(String.format(" #%s\n", w.toString())));
                return output.toString().trim();
            case "Priority":
                workItemsList = new ArrayList<>();
                engine.workItems.forEach((id, workitem) -> workItemsList.add(workitem));

                List<Assignable> assignables = new ArrayList<>();
                for (WorkItem workItem : workItemsList) {
                    if (workItem instanceof BugImpl) {
                        Assignable bug = (Assignable) workItem;
                        assignables.add(bug);
                    } else if (workItem instanceof StoryImpl) {
                        Assignable story = (Assignable) workItem;
                        assignables.add(story);
                    }
                }

                output = new StringBuilder(String.format(Constants.WORK_ITEMS_SORTED_BY_S, "PriorityType"));
                assignables.sort(Comparator.comparing(Assignable::getPriorityType));
                assignables.forEach(w -> output.append(String.format(" #%s\n", w.toString())));

                return output.toString().trim();
            case "Rating":
                workItemsList = new ArrayList<>();
                List<WorkItem> filteredWorkItemList;
                List<Feedback> feedbacks = new ArrayList<>();

                engine.workItems.forEach((id, workItem) -> workItemsList.add(workItem));
                filteredWorkItemList = workItemsList.stream().filter(w -> w instanceof FeedbackImpl)
                        .collect(Collectors.toList());
                for (WorkItem workitem : filteredWorkItemList) {
                    Feedback feedback = (FeedbackImpl) workitem;
                    feedbacks.add(feedback);
                }
                feedbacks.sort(Comparator.comparingInt(Feedback::getRating));

                output = new StringBuilder(String.format(Constants.WORK_ITEMS_SORTED_BY_S, "Rating"));
                feedbacks.forEach(f -> output.append(String.format(" #%s\n", f.toString())));
                return output.toString().trim();
            case "Size":
                workItemsList = new ArrayList<>();
                List<Story> storyes = new ArrayList<>();

                engine.workItems.forEach((id, workItem) -> workItemsList.add(workItem));
                filteredWorkItemList = workItemsList.stream().filter(w -> w instanceof StoryImpl)
                        .collect(Collectors.toList());
                for (WorkItem workItem : filteredWorkItemList) {
                    Story story = (StoryImpl) workItem;
                    storyes.add(story);
                }
                storyes.sort(Comparator.comparing(Story::getSize).reversed());
                output = new StringBuilder(String.format(Constants.WORK_ITEMS_SORTED_BY_S, "Size"));
                storyes.forEach(s -> output.append(String.format(" #%s\n", s.toString())));
                return output.toString().trim();
            case "Severity":
                workItemsList = new ArrayList<>();
                List<Bug> bugs = new ArrayList<>();

                engine.workItems.forEach((id, workItem) -> workItemsList.add(workItem));
                filteredWorkItemList = workItemsList.stream().filter(w -> w instanceof BugImpl)
                        .collect(Collectors.toList());
                for (WorkItem workItem : filteredWorkItemList) {
                    Bug bug = (BugImpl) workItem;
                    bugs.add(bug);
                }
                bugs.sort(Comparator.comparing(Bug::getSeverityType));
                output = new StringBuilder(String.format(Constants.WORK_ITEMS_SORTED_BY_S, "Severity"));
                bugs.forEach(b -> output.append(String.format(" #%s\n", b.toString())));
                return output.toString().trim();
            default:
                return Constants.INVALID_SORTING_OPTION;
        }
    }

    public StatusType getStatusType(String statusType) {
        StatusType status;
        try {
            status = StatusType.valueOf(statusType);
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format(Constants.INVALID_ARGUMENT_S, "StatusType"));
        }
        return status;
    }

    public PriorityType getPriorityType(String priorityType) {
        PriorityType priority;
        try {
            priority = PriorityType.valueOf(priorityType);
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format(Constants.INVALID_ARGUMENT_S, "PriorityType"));
        }
        return priority;
    }

    public SeverityType getSeverityType(String severityType) {
        SeverityType severity;
        try {
            severity = SeverityType.valueOf(severityType);
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format(Constants.INVALID_ARGUMENT_S, "Severity"));
        }
        return severity;
    }

    public SizeType getSizeType(String sizeType) {
        SizeType size;
        try {
            size = SizeType.valueOf(sizeType);
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format(Constants.INVALID_ARGUMENT_S, "Size"));
        }
        return size;
    }

    private void emptyStringBuilderValidation(StringBuilder str, String errorMessage) {
        if (str.length() == 0) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    private void mapHasKey(Map map, String key, String option, String errorMessage) {
        if (option.equals("yes")) {
            if (map.containsKey(key)) {
                throw new IllegalArgumentException(errorMessage);
            }
        } else if (option.equals("no")) {
            if (!map.containsKey(key)) {
                throw new IllegalArgumentException(errorMessage);
            }
        }
    }

    private void mapIsEmpty(Map map, String errorMessage) {
        if (map.isEmpty()) {
            throw new IllegalArgumentException(errorMessage);
        }
    }
}
