package com.telerikacademy.workitemmanagement.models;

public class Constants {

    //Engine constants
    static final String INVALID_COMMAND = "Invalid command name: %s!";
    static final String S_EXISTS = "%s %s already exists!";
    static final String S_DOESNT_EXIST = "%s %s doesn't exist!";
    static final String S_CREATED = "%s %s created!";
    static final String NO_YET = "No %s yet!";
    static final String ALL_S_LIST = "All %s list:\n";
    static final String TEAM_S_ACTIVITY = "Team %s activity: ";
    static final String MEMBER_S_ADDED_TO_TEAM_S = "Member %s added to team %s";
    static final String BOARD_S_CREATED_IN_TEAM_S = "Board %s created in team %s";
    static final String TEAM_S_BOARDS = "Team %s boards:\n";
    static final String S_CREATED_IN_BOARD_S = "%s with title %s created in board %s";
    static final String INVALID_ARGUMENT_S = "Invalid argument: %s";
    static final String CHANGED = "%s %s %s is changed to: %s";
    static final String WORK_ITEM_DOES_NOT_HAVE = "Work item does not have a %s!";
    static final String WORK_ITEM_ASSIGNED_TO_S = "Work item with ID %s assigned to %s";
    static final String WORK_ITEM_UNASSIGNED_S = "Work item with ID %s unassigned of member %s";
    static final String COMMENT_ADDED_TO_WORKITEM = "Comment added to work item with ID %s:\n" + " -%s";
    static final String NO_WORK_ITEMS_FOUND = "No work items found!";
    static final String NO_S_IN_WORKITEMS = "No Workitem %s in Workitems";
    static final String ALL_WORK_ITEMS_WITH_STATUS_TYPE_S = "All work items with StatusType.%s:\n";
    static final String WORK_ITEMS_SORTED_BY_S = "Work items sorted by %s:\n";
    static final String INVALID_SORTING_OPTION = "Invalid sorting option!";
    static final String ILLEGAL = "Illegal %s";
    static final String NO_WORK_ITEMS_ASSIGNED = "%s has no work items assigned";
    static final String MEMBER_S_WORK_ITEMS = "Member %s work items:\n";
    static final String S_HAS_NO_WORK_ITEMS_ASSIGNED_WITH_STATUS_TYPE_S = "%s has no work items assigned with StatusType.%s";
    static final String MEMBER_S_WORK_ITEMS_WITH_STATUS_TYPE_S = "Member %s work items with StatusType.%s:\n";
    static final String FEEDBACK_TO_A_MEMBER_ASSIGN_ERROR_MESSAGE = "Cannot assign Feedback to a member!";
    static final String FEEDBACK_TO_A_MEMBER_UNASSIGN_ERROR_MESSAGE = "Member cannot have a Feedback assigned!";

    //Team constants
    static final String EMPTY_TEAM_MESSAGE = "No %s in this team";
    static final String CREATED_NEW_TEAM_MESSAGE = "%s was created%n";
    static final String MEMBER_ADDED_MESSAGE = "%s has been added to %s";
    static final String BOARD_ALREADY_EXISTS_ERROR_MESSAGE = "Board already exists in %s";
    static final String MEMBER_ALREADY_EXISTS_ERROR_MESSAGE = "Member %s is already in team %s";
    static final String MEMBERS_MESSAGE = "members";
    static final String BOARDS_MESSAGE = "boards";
    static final String SHOW_TEAM_MEMBERS_MESSAGE = "Team %s %s:%n";
    static final String NO_BOARDS = "Team %s has no Boards!";

    //WorkItemAbstract constants
    static final int WORK_ITEM_MIN_LENGTH = 10;
    static final int TITLE_MAX_LENGTH = 50;
    static final int DESCRIPTION_MAX_LENGTH = 500;
    static final String TITLE_LENGTH_MESSAGE = "Title should be between 10 and 50 symbols";
    static final String DESCRIPTION_LENGTH_MESSAGE = "Description should be between 10 and 500 symbols";
    static final String STATUS_TYPE_SET_MESSAGE = "Status type is set to %s";
    static final String STATUS_TYPE_ERROR_MESSAGE = "StatusType is not valid ";
    static final String EMPTY_LIST_ERROR_MESSAGE = "Work item has no %s yet";
    static final String ADDED_TO_THIS_BOARD_MESSAGE = "added %s to this board";

    //Assignable WorkItems constants
    static final String WORK_ITEM_ALREADY_HAS_ASIGNEE_MESSAGE = "Work item with ID %s already has an asignee: %s!";
    static final String ASSIGNEE_IS_SET_MESSAGE = "Assignee %s is set";
    static final String ASSIGNEE_IS_REMOVED_MESSAGE = "Assignee %s is unset";
    static final String NOT_ASSIGNED_MESSAGE = "%s is not assigned to %s";
    static final String NO_MEMBER_ASSIGNED_MESSAGE = "Workitme %s has no member assigned";
    static final String PRIORITY_TYPE_ERROR_MESSAGE = "PriorityType is not valid";
    static final String PRIORITY_TYPE_SET_MESSAGE = "Priority type was set to %s";
    static final String ASSIGNEE_MESSAGE = " Assignee: %s;";

    //Story constants
    static final String SIZE_TYPE_ERROR_MESSAGE = "SizeTypeNotValid";
    static final String SIZE_TYPE_SET_MESSAGE = "Size type is set to %s";
    static final String SIZE_MESSAGE = " Size: %s";

    //Bug constants
    static final String SEVERITY_TYPE_ERROR_MESSAGE = "SeverityTypeNotValid";
    static final String SEVERITY_TYPE_SET_MESSAGE = "Severity type was set to %s";
    static final String SEVERITY_TYPE_MESSAGE = " SeverityType: %s";

    //Feedback constants
    static final String RATING_SET_MESSAGE = "rating is set to %s";
    static final int ZERO = 0;
    static final String RATING_MESSAGE = " Rating %d";
    static final String RATING_CANNOT_BE_LESS_THAN_ZERO_MESSAGE = "Rating cannot be less than 0";

    //Member and board base constants
    static final String NAME_LENGTH_ERROR_MESSAGE = "Name should be more than 5 and less than %d characters long";
    static final String NO_WORK_ITEMS_FOUND_MESSAGE = "No work items found";
    static final String CREATED_MESSAGE = "%s created";
    static final int BOARD_MEMBER_NAME_MIN_LENGTH = 5;
    static final int MAX_LENGTH = 15;

    //Member constants
    static final String ITEM_NOT_FOUND_ERROR_MESSAGE = "Error: Member %s does not have a work item with ID %s assigned!";
    static final String MEMBER_JOINED_IN_WORK_ITEM_MESSAGE = "%s joined %s";
    static final String MEMBER_LEFT_WORK_ITEM_MESSAGE = "%s left %s";
    static final String MEMBER_MESSAGE = "Member";

    //Board constants
    static final String ADDING_WORK_ITEM_TO_BOARD_MESSAGE = "adding %s to this board";
    static final int BOARD_NAME_MAX_LENGTH = 10;

    //Command constants
    static final char SEPARATOR_SYMBOL = ' ';
    static final String NAME_CANNOT_BE_NULL_OR_EMPTY_MESSAGE = "Name cannot be null or empty";
    static final String PARAMETERS_LIST_CANNOT_BE_NULL_MESSAGE = "Parameters list cannot be null";

}
