package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.contracts.Board;
import com.telerikacademy.workitemmanagement.contracts.Member;
import com.telerikacademy.workitemmanagement.contracts.Team;
import java.util.HashMap;
import java.util.Map;

public class TeamImpl implements Team {

    private String name;
    private Map<String, Member> members;
    private Map<String, Board> boards;

    public TeamImpl(String name) {
        setName(name);
        this.members = new HashMap();
        this.boards = new HashMap();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Map<String, Member> getMembers() {
        ifEmpty(members, Constants.MEMBERS_MESSAGE);
        return new HashMap<>(members);
    }

    @Override
    public Map<String, Board> getBoards() {
        if(boards.size() < 1){
            throw new IllegalArgumentException(String.format(Constants.NO_BOARDS, getName()));
        }
        return new HashMap<>(boards);
    }

    @Override
    public String showTeamActivity() {
        StringBuilder output = new StringBuilder();
        output.append(String.format(Constants.CREATED_NEW_TEAM_MESSAGE, getName()));
        return output.toString();
    }

    @Override
    public String showTeamMembers() {
        ifEmpty(members, Constants.MEMBERS_MESSAGE);
        StringBuilder output = new StringBuilder();
        output.append(String.format(Constants.SHOW_TEAM_MEMBERS_MESSAGE, getName(), Constants.MEMBERS_MESSAGE));
        members.forEach((memberName, member) -> output
                .append(String.format(" #%s%n", member.getName())));
        return output.toString();
    }

    @Override
    public String showTeamBoards() {
        ifEmpty(boards, Constants.BOARDS_MESSAGE);
        StringBuilder output = new StringBuilder();
        boards.forEach((boardName, board) -> output.append(boardName));
        return output.toString();
    }

    @Override
    public void addMember(Member member) {
        checkIfMemberExists(member);
        member.addActivity(String.format(Constants.MEMBER_ADDED_MESSAGE, member.getName(), this.name));
        members.put(member.getName(), member);
    }

    @Override
    public Board createNewBoard(String boardName) {
        checkIfBoardExists(boardName);
        Board newBoard = new BoardImpl(boardName);
        boards.put(boardName, newBoard);
        return newBoard;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void checkIfBoardExists(String boardName) {
        if (boards.containsKey(boardName)) {
            throw new IllegalArgumentException(String.format
                    (Constants.BOARD_ALREADY_EXISTS_ERROR_MESSAGE, getName()));
        }
    }

    private void checkIfMemberExists(Member member) {
        if (members.containsKey(member.getName())) {
            throw new IllegalArgumentException(String.format
                    (Constants.MEMBER_ALREADY_EXISTS_ERROR_MESSAGE, member.getName(), this.getName()));
        }
    }

    private void ifEmpty(Map map, String message) {
        if (map.isEmpty()) {
            throw new IllegalArgumentException(String.format
                    (Constants.EMPTY_TEAM_MESSAGE, message));
        }
    }
}