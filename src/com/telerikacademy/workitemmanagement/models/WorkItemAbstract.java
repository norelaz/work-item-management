package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.enums.StatusType;
import com.telerikacademy.workitemmanagement.contracts.WorkItem;
import java.util.ArrayList;
import java.util.List;

public abstract class WorkItemAbstract implements WorkItem {

    private String id;
    private String title;
    private String description;
    private StatusType statusType;
    private List<String> comments;
    private List<String> history;

    WorkItemAbstract(String id, String title, String description, StatusType statusType) {
        comments = new ArrayList<>();
        history = new ArrayList<>();
        setId(id);
        setTitle(title);
        setDescription(description);
        setStatusType(statusType);
        addHistory(String.format(Constants.ADDED_TO_THIS_BOARD_MESSAGE, getTitle()));
    }

    @Override
    public StatusType getStatusType() {
        return statusType;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<String> getComments() {
        listEmpty(comments, "comments");
        return new ArrayList<>(comments);
    }

    @Override
    public void addComment(String comment) {
        comments.add(comment);
    }

    @Override
    public List<String> getHistory() {
        listEmpty(history, "history");
        return new ArrayList<>(history);
    }

    @Override
    public String toString() {
        printHistory();
        return String.format(
                "  ID: %s," +
                        " Title: %s," +
                        " Description: %s," +
                        " StatusType: %s,\n" +
                        "%s," +
                        "%s.",
                getId(), getTitle(), getDescription(), getStatusType(), printHistory() , additionalInfo()).trim();
    }



    public void setStatusType(StatusType statusType) {
        statusTypeValidation(statusType);
        this.statusType = statusType;
        addHistory(String.format(Constants.STATUS_TYPE_SET_MESSAGE, getStatusType()));
    }

    protected void addHistory(String newHistory) {
        history.add(String.format("   %s,\n", newHistory));
    }

    protected abstract String additionalInfo();

    protected abstract List<StatusType> getStatusPossibleTypes();

    private void setTitle(String title) {
        stringLengthValidation(title, Constants.TITLE_MAX_LENGTH, Constants.TITLE_LENGTH_MESSAGE);
        this.title = title;
    }

    private void setId(String id) {
        this.id = id;
    }

    private void setDescription(String description) {
        stringLengthValidation(description, Constants.DESCRIPTION_MAX_LENGTH, Constants.DESCRIPTION_LENGTH_MESSAGE);
        this.description = description;
    }

    private StringBuilder printHistory() {
        StringBuilder history = new StringBuilder("   History: \n");
        getHistory().forEach(history::append);
        history.deleteCharAt(history.length() - 1);
        history.deleteCharAt(history.length() - 1);
        return history;
    }

    private void statusTypeValidation(StatusType statusType){
        if (!getStatusPossibleTypes().contains(statusType)) {
            throw new IllegalArgumentException(Constants.STATUS_TYPE_ERROR_MESSAGE);
        }
    }

    private void stringLengthValidation(String string, int maxLength, String lengthErrorMessage) {
        if (string.length() < Constants.WORK_ITEM_MIN_LENGTH || string.length() > maxLength) {
            throw new IllegalArgumentException(lengthErrorMessage);
        }
    }

    private void listEmpty(List list, String message) {
        if (list.isEmpty()) {
            throw new IllegalArgumentException(String.format(Constants.EMPTY_LIST_ERROR_MESSAGE, message));
        }
    }
}
