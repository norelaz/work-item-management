package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.contracts.Command;

import java.util.ArrayList;
import java.util.List;

public class CommandImpl implements Command {


    private String commandName;
    private List<String> parameters;

    static CommandImpl parse(String input) {
        return new CommandImpl(input);
    }

    private CommandImpl(String input) {
        translateInput(input);
    }

    public String getCommandName() {
        return commandName;
    }

    public List<String> getParameters() {
        return parameters;
    }

    private void setParameters(List<String> parameters) {
        if (parameters == null) {
            throw new IllegalArgumentException(Constants.PARAMETERS_LIST_CANNOT_BE_NULL_MESSAGE);
        }
        this.parameters = parameters;
    }

    private void setCommandName(String commandName) {
        if (commandName == null || commandName.isEmpty()) {
            throw new IllegalArgumentException(Constants.NAME_CANNOT_BE_NULL_OR_EMPTY_MESSAGE);
        }
        this.commandName = commandName;
    }

    private void translateInput(String input) {
        int indexOfFirstSeparator = input.indexOf(Constants.SEPARATOR_SYMBOL);

        if (indexOfFirstSeparator < 0) {
            setCommandName(input);
            return;
        }
        setCommandName(input.substring(0, indexOfFirstSeparator));

        String[] lines = input.substring(indexOfFirstSeparator + 1).split(Character.toString(Constants.SEPARATOR_SYMBOL));

        List<String> actualParameters = new ArrayList<>();

        for (String parameter : lines) {
            if (!parameter.isEmpty())
                actualParameters.add(parameter);
        }

        this.setParameters(actualParameters);
    }
}
