package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.contracts.Assignable;
import com.telerikacademy.workitemmanagement.contracts.Member;
import com.telerikacademy.workitemmanagement.contracts.WorkItem;

public class MemberImpl extends MemberAndBoardAbstract implements Member {

    public MemberImpl(String name) {
        super(name);
    }

    @Override
    public void assignWorkItem(Assignable workItem) {
        workItem.setAssignee(getName());
        getOriginalWorkitems().add(workItem);
        getOriginalHistory().add(String.format(Constants.MEMBER_JOINED_IN_WORK_ITEM_MESSAGE, getName(), workItem.getTitle()));
    }

    @Override
    public void unassignWorkItem(Assignable workItem) {
        workItemExists(workItem);
        workItem.unSetAssignee(getName());
        getOriginalWorkitems().remove(workItem);
        getOriginalHistory().add(String.format(Constants.MEMBER_LEFT_WORK_ITEM_MESSAGE, getName(), workItem.getTitle()));
    }

    @Override
    protected String memberOrBoardCreation() {
        return Constants.MEMBER_MESSAGE;
    }

    private void workItemExists(WorkItem workItem) {
        if (!getOriginalWorkitems().contains(workItem)) {
            throw new IllegalArgumentException(String.format(Constants.ITEM_NOT_FOUND_ERROR_MESSAGE, getName(), workItem.getId()));
        }
    }
}
