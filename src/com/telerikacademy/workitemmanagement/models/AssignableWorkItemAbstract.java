package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.contracts.Assignable;
import com.telerikacademy.workitemmanagement.enums.PriorityType;
import com.telerikacademy.workitemmanagement.enums.StatusType;
import java.util.ArrayList;
import java.util.List;

public abstract class AssignableWorkItemAbstract extends WorkItemAbstract implements Assignable {

    private List<String> members;
    private PriorityType priorityType;

    public AssignableWorkItemAbstract(String id, String title, String description, StatusType statusType, PriorityType priorityType) {
        super(id, title, description, statusType);
        setPriorityType(priorityType);
        members = new ArrayList<>();
    }

    @Override
    public String getAssignee() {
        if (members.size() < 1) {
            return String.format(Constants.NO_MEMBER_ASSIGNED_MESSAGE, getTitle());
        }
        return members.get(0);
    }

    @Override
    public void setAssignee(String member) {
        if (members.size() == 1) {
            throw new IllegalArgumentException(String.format(Constants.WORK_ITEM_ALREADY_HAS_ASIGNEE_MESSAGE, getId(), members.get(0)));
        }
        members.add(member);
        addHistory(String.format(Constants.ASSIGNEE_IS_SET_MESSAGE, member));
    }

    @Override
    public void unSetAssignee(String member) {
        if (!members.contains(member)) {
            throw new IllegalArgumentException(String.format(Constants.NOT_ASSIGNED_MESSAGE, member, getTitle()));
        }
        this.members.remove(0);
        addHistory(String.format(Constants.ASSIGNEE_IS_REMOVED_MESSAGE, member));
    }

    public PriorityType getPriorityType() {
        return priorityType;
    }

    public void setPriorityType(PriorityType priorityType) {
        priorityTypeValidation(priorityType);
        this.priorityType = priorityType;
        addHistory(String.format(Constants.PRIORITY_TYPE_SET_MESSAGE, getPriorityType()));
    }

    protected abstract List<StatusType> getStatusPossibleTypes();

    @Override
    protected String additionalInfo() {
        return String.format(Constants.ASSIGNEE_MESSAGE, getAssignee());
    }

    private void priorityTypeValidation(PriorityType priorityType) {
        if (priorityType != PriorityType.HIGH && priorityType != PriorityType.MEDIUM && priorityType != PriorityType.LOW) {
            throw new IllegalArgumentException(Constants.PRIORITY_TYPE_ERROR_MESSAGE);
        }
    }
}
