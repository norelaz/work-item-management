package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.contracts.*;

import java.util.*;

public class EngineImpl implements Engine {

    final Factory factory;
    final Map<String, Team> teams;
    final Map<String, Member> members;
    final Map<String, Board> boards;
    final Map<String, WorkItem> workItems;

    public EngineImpl() {
        factory = new FactoryImpl();
        teams = new HashMap<>();
        members = new HashMap<>();
        boards = new HashMap<>();
        workItems = new HashMap<>();
    }

    @Override
    public void Start() {
        List commands = readCommands();
        List commandResult = processCommands(commands);
        printReports(commandResult);
    }

    private List<Command> readCommands() {
        List commands = new ArrayList<Command>();

        Scanner scanner = new Scanner(System.in);
        String currentLine = scanner.nextLine();

        while (currentLine != null && !currentLine.isEmpty()) {
            Command currentCommand = CommandImpl.parse(currentLine);
            commands.add(currentCommand);
            currentLine = scanner.nextLine();
        }

        return commands;
    }

    private List<String> processCommands(List<Command> commands) {
        List<String> reports = new ArrayList<>();
        ProcessSingleCommandImpl processSingleCommandImpl = new ProcessSingleCommandImpl();

        for (Command command : commands) {
            try {
                String report = processSingleCommandImpl.process(command);
                reports.add(report);
            } catch (Exception ex) {
                reports.add(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
            }
        }

        return reports;
    }

    private void printReports(List<String> reports) {
        StringBuilder output = new StringBuilder();
        for (String report : reports) {
            output.append(String.format("%s\n", report));
        }

        System.out.print(output.toString());
    }
}
