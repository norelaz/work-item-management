package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.contracts.AllCommands;
import com.telerikacademy.workitemmanagement.contracts.Command;
import com.telerikacademy.workitemmanagement.contracts.ProcessSingleCommand;
import com.telerikacademy.workitemmanagement.enums.PriorityType;
import com.telerikacademy.workitemmanagement.enums.SeverityType;
import com.telerikacademy.workitemmanagement.enums.SizeType;
import com.telerikacademy.workitemmanagement.enums.StatusType;

public class ProcessSingleCommandImpl implements ProcessSingleCommand {
    private final AllCommands allCommandsImpl;

    public ProcessSingleCommandImpl() {
        allCommandsImpl = new AllCommandsImpl();
    }

    public String process(Command command) {
        switch (command.getCommandName()) {
            case "CreateNewMember":
                String memberName = command.getParameters().get(0);
                return allCommandsImpl.createMember(memberName);
            case "ShowAllPeople":
                return allCommandsImpl.showAllPeople();
            case "ShowMemberActivity":
                memberName = command.getParameters().get(0);
                return allCommandsImpl.showMemberActivity(memberName);
            case "CreateNewTeam":
                String teamName = command.getParameters().get(0);
                return allCommandsImpl.createTeam(teamName);
            case "ShowAllTeams":
                return allCommandsImpl.showAllTeams();
            case "ShowTeamActivity":
                teamName = command.getParameters().get(0);
                return allCommandsImpl.showTeamActivity(teamName);
            case "AddMemberToTeam":
                memberName = command.getParameters().get(0);
                teamName = command.getParameters().get(1);
                return allCommandsImpl.addMemberToTeam(memberName, teamName);
            case "ShowAllTeamMembers":
                teamName = command.getParameters().get(0);
                return allCommandsImpl.showAllTeamMembers(teamName);
            case "CreateABoardInTeam":
                String boardName = command.getParameters().get(0);
                teamName = command.getParameters().get(1);
                return allCommandsImpl.createABoardInTeam(boardName, teamName);
            case "ShowAllTeamBoards":
                teamName = command.getParameters().get(0);
                return allCommandsImpl.showAllTeamBoards(teamName);
            case "ShowBoardActivity":
                boardName = command.getParameters().get(0);
                return allCommandsImpl.showBoardActivity(boardName);
            case "CreateNewBugInBoard":
                boardName = command.getParameters().get(0);
                String id = command.getParameters().get(1);
                String title = command.getParameters().get(2);
                String description = command.getParameters().get(3);
                StatusType statusType = allCommandsImpl.getStatusType(command.getParameters().get(4));
                PriorityType priorityType = allCommandsImpl.getPriorityType(command.getParameters().get(5));
                SeverityType severityType = allCommandsImpl.getSeverityType(command.getParameters().get(6));
                return allCommandsImpl.createNewBugInBoard(boardName, id, title, description, statusType, priorityType, severityType);
            case "CreateNewStoryInBoard":
                boardName = command.getParameters().get(0);
                id = command.getParameters().get(1);
                title = command.getParameters().get(2);
                description = command.getParameters().get(3);
                statusType = allCommandsImpl.getStatusType(command.getParameters().get(4));
                priorityType = allCommandsImpl.getPriorityType(command.getParameters().get(5));
                SizeType sizeType = allCommandsImpl.getSizeType(command.getParameters().get(6));
                return allCommandsImpl.createNewStoryInBoard(boardName, id, title, description, statusType, priorityType, sizeType);
            case "CreateNewFeedbackInBoard":
                boardName = command.getParameters().get(0);
                id = command.getParameters().get(1);
                title = command.getParameters().get(2);
                description = command.getParameters().get(3);
                statusType = allCommandsImpl.getStatusType(command.getParameters().get(4));
                int rating = Integer.parseInt(command.getParameters().get(5));
                return allCommandsImpl.createNewFeedBackInBoard(boardName, id, title, description, statusType, rating);
            case "Change":
                id = command.getParameters().get(0);
                String feature = command.getParameters().get(1);
                return allCommandsImpl.changeWorkItemfeature(id, feature);
            case "Assign":
                memberName = command.getParameters().get(0);
                id = command.getParameters().get(1);
                return allCommandsImpl.assignWorkItemToMember(memberName, id);
            case "Unassign":
                memberName = command.getParameters().get(0);
                id = command.getParameters().get(1);
                return allCommandsImpl.unassignWorkItemToMember(memberName, id);
            case "AddComment":
                id = command.getParameters().get(0);
                String comment = command.getParameters().get(1);
                return allCommandsImpl.addCommentToAWorkItem(id, comment);
            case "ListAll":
                return allCommandsImpl.listAll();
            case "FilterByInstance":
                String filteringArgument = command.getParameters().get(0);
                return allCommandsImpl.filterByInstance(filteringArgument);
            case "FilterByStatus":
                String type = command.getParameters().get(0);
                return allCommandsImpl.filterByStatus(type);
            case "FilterByAssignee":
                memberName = command.getParameters().get(0);
                return allCommandsImpl.filterByAssignee(memberName);
            case "FilterByAssigneeAndStatus":
                memberName = command.getParameters().get(0);
                type = command.getParameters().get(1);
                return allCommandsImpl.filterByStatusAndAssignee(memberName, type);
            case "ListSortedBy":
                String sortingOption = command.getParameters().get(0);
                return allCommandsImpl.listSortedBy(sortingOption);

            default:
                return String.format(Constants.INVALID_COMMAND, command.getCommandName());
        }
    }
}
