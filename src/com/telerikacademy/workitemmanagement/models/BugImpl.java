package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.enums.PriorityType;
import com.telerikacademy.workitemmanagement.enums.SeverityType;
import com.telerikacademy.workitemmanagement.enums.StatusType;
import com.telerikacademy.workitemmanagement.contracts.Bug;
import java.util.ArrayList;
import java.util.List;

public class BugImpl extends AssignableWorkItemAbstract implements Bug {

    private List<String> steps;
    private SeverityType severityType;

    public BugImpl(String id, String title, String description, StatusType statusType,
                   PriorityType priorityType, SeverityType severityType) {
        super(id, title, description, statusType, priorityType);
        setSeverityType(severityType);
        steps = new ArrayList<>();
    }

    public List<String> getSteps() {
        return steps;
    }

    @Override
    public SeverityType getSeverityType() {
        return severityType;
    }

    public void setSeverityType(SeverityType severityType) {
        severityTypeValidation(severityType);
        this.severityType = severityType;
        addHistory(String.format(Constants.SEVERITY_TYPE_SET_MESSAGE, getSeverityType()));
    }

    @Override
    protected String additionalInfo() {
        return String.format(Constants.SEVERITY_TYPE_MESSAGE, getSeverityType());
    }

    @Override
    protected List<StatusType> getStatusPossibleTypes() {
        List<StatusType> statusTypes = new ArrayList<>();
        statusTypes.add(StatusType.ACTIVE);
        statusTypes.add(StatusType.FIXED);
        return statusTypes;
    }

    private void severityTypeValidation(SeverityType severityType) {
        if (severityType != SeverityType.CRITICAL && severityType != SeverityType.MAJOR && severityType != SeverityType.MINOR) {
            throw new IllegalArgumentException(Constants.SEVERITY_TYPE_ERROR_MESSAGE);
        }
    }
}
