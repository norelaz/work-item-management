package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.enums.PriorityType;
import com.telerikacademy.workitemmanagement.enums.SizeType;
import com.telerikacademy.workitemmanagement.enums.StatusType;
import com.telerikacademy.workitemmanagement.contracts.Story;
import java.util.ArrayList;
import java.util.List;

public class StoryImpl extends AssignableWorkItemAbstract implements Story {

    SizeType size;

    public StoryImpl(String id, String title, String description, StatusType statusType,
                     PriorityType priorityType, SizeType size) {
        super(id, title, description, statusType, priorityType);
        setSize(size);
    }

    @Override
    public SizeType getSize() {
        return size;
    }

    @Override
    public void setSize(SizeType size) {
        sizeTypeValidation(size);
        this.size = size;
        addHistory(String.format(Constants.SIZE_TYPE_SET_MESSAGE, getSize()));
    }

    @Override
    protected String additionalInfo() {
        return String.format(Constants.SIZE_MESSAGE, getSize());
    }

    @Override
    protected List<StatusType> getStatusPossibleTypes() {
        List<StatusType> list = new ArrayList<>();
        list.add(StatusType.NOTDONE);
        list.add(StatusType.INPROGRESS);
        list.add(StatusType.DONE);
        return list;
    }

    void sizeTypeValidation(SizeType size) {
        if(size != SizeType.LARGE && size != SizeType.MEDIUM && size != SizeType.SMALL){
            throw new IllegalArgumentException(Constants.SIZE_TYPE_ERROR_MESSAGE);
        }
    }
}
