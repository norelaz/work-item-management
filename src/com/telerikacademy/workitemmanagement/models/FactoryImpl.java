package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.contracts.Factory;
import com.telerikacademy.workitemmanagement.contracts.Member;
import com.telerikacademy.workitemmanagement.contracts.Team;

public class FactoryImpl implements Factory {
    @Override
    public Member createMember(String name) {
        return new MemberImpl(name);
    }

    @Override
    public Team createTeam(String name) {
        return new TeamImpl(name);
    }
}

