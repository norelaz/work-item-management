package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.contracts.MemberAndBoard;
import com.telerikacademy.workitemmanagement.contracts.WorkItem;
import java.util.ArrayList;
import java.util.List;

public abstract class MemberAndBoardAbstract implements MemberAndBoard {

    private String name;
    private List<WorkItem> workItems;
    private List<String> history;

    MemberAndBoardAbstract(String name) {
        setName(name);
        this.workItems = new ArrayList<>();
        this.history = new ArrayList<>();
        history.add(String.format(Constants.CREATED_MESSAGE, memberOrBoardCreation()));
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<WorkItem> getWorkItems() {
        emptyListValidation(workItems);
        return new ArrayList<>(workItems);
    }

    @Override
    public String getHistory() {
        StringBuilder output = new StringBuilder(String.format("%s %s history:%n", memberOrBoardCreation(), getName()));
        history.forEach(h -> output.append(String.format(" -%s%n", h)));
        return output.toString().trim();
    }

    @Override
    public String toString() {
        printWorkitems();
        return String.format("%s\n Workitems: %s", getName(), printWorkitems());
    }

    @Override
    public void addActivity(String activity) {
        history.add(String.format("%s", activity));
    }

    protected abstract String memberOrBoardCreation();

    protected List<WorkItem>getOriginalWorkitems(){
        return workItems;
    }

    protected List<String>getOriginalHistory(){
        return history;
    }

    protected int getMaxLength() {
        return Constants.MAX_LENGTH;
    }

    private void setName(String name) {
        nameValidation(name);
        this.name = name;
    }

    private String printWorkitems() {
        StringBuilder output = new StringBuilder();
        workItems.forEach(w -> output.append(String.format("%s, ", w.getTitle())));
        if(output.length() == 0){
            output.append("No work items assigned  ");
        }
        output.deleteCharAt(output.length() - 1);
        output.deleteCharAt(output.length() - 1);
        return output.toString();
    }

    private void nameValidation(String name) {
        if (name.length() < Constants.BOARD_MEMBER_NAME_MIN_LENGTH || name.length() > getMaxLength()) {
            throw new IllegalArgumentException (String.format(Constants.NAME_LENGTH_ERROR_MESSAGE, getMaxLength()));
        }
    }

    private void emptyListValidation(List list) {
        if (list.isEmpty()) {
            throw new IllegalArgumentException(Constants.NO_WORK_ITEMS_FOUND_MESSAGE);
        }
    }

}
