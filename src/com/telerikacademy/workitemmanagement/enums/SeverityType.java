package com.telerikacademy.workitemmanagement.enums;

public enum SeverityType {
    MINOR,
    MAJOR,
    CRITICAL;

    @Override
    public String toString() {
        switch (this) {
            case CRITICAL:
                return "Critical";
            case MAJOR:
                return "Major";
            case MINOR:
                return "Minor";
            default:
                throw new IllegalArgumentException();
        }
    }
}
