package com.telerikacademy.workitemmanagement.enums;

public enum StatusType {
    ACTIVE,
    FIXED,
    NOTDONE,
    INPROGRESS,
    DONE,
    NEW,
    UNSCHEDULED,
    SCHEDULED;

    @Override
    public String toString() {
        switch (this) {
            case ACTIVE:
                return "Active";
            case FIXED:
                return "Fixed";
            case NOTDONE:
                return "NotDone";
            case INPROGRESS:
                return "InProgress";
            case DONE:
                return "Done";
            case NEW:
                return "New";
            case UNSCHEDULED:
                return "Unscheduled";
            case SCHEDULED:
                return "Scheduled";
            default:
                throw new IllegalArgumentException();
        }
    }
}
