package com.telerikacademy.workitemmanagement;

import com.telerikacademy.workitemmanagement.contracts.Engine;
import com.telerikacademy.workitemmanagement.models.EngineImpl;

public class Startup {
    public static void main(String[] args) {
        Engine engine = new EngineImpl();
        engine.Start();
    }
}
