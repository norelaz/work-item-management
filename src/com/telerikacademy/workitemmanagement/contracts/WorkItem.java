package com.telerikacademy.workitemmanagement.contracts;

import com.telerikacademy.workitemmanagement.enums.StatusType;

import java.util.List;

public interface WorkItem {
    String getId();

    String getTitle();

    String getDescription();

    StatusType getStatusType();

    List<String> getComments();

    List<String> getHistory();

    void addComment(String comment);

    void setStatusType(StatusType statusType);
}
