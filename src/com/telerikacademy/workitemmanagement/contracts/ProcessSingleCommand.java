package com.telerikacademy.workitemmanagement.contracts;

public interface ProcessSingleCommand {
    String process(Command command);
}
