package com.telerikacademy.workitemmanagement.contracts;

public interface Feedback extends WorkItem{
    int getRating();

    void setRating(int raing);
}
