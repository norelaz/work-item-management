package com.telerikacademy.workitemmanagement.contracts;

public interface Member extends MemberAndBoard{
    void assignWorkItem(Assignable workItem);

    void unassignWorkItem(Assignable workItem);
}
