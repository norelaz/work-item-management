package com.telerikacademy.workitemmanagement.contracts;

import com.telerikacademy.workitemmanagement.enums.PriorityType;
import com.telerikacademy.workitemmanagement.enums.SizeType;

public interface Story extends Assignable {
    PriorityType getPriorityType();

    SizeType getSize();

    void setPriorityType(PriorityType priorityType);

    void setSize(SizeType size);
}
