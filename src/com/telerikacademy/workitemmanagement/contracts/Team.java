package com.telerikacademy.workitemmanagement.contracts;

import java.util.Map;

public interface Team {
    String getName();

    Map<String ,Member> getMembers();

    Map<String ,Board> getBoards();

    String showTeamActivity();

    String showTeamMembers();

    String showTeamBoards();

    void addMember(Member member);

    Board createNewBoard(String board);
}
