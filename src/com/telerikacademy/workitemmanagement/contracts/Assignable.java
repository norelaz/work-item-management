package com.telerikacademy.workitemmanagement.contracts;

import com.telerikacademy.workitemmanagement.enums.PriorityType;

public interface Assignable extends WorkItem {
    void setPriorityType(PriorityType priorityType);

    PriorityType getPriorityType();

    void setAssignee(String member);

    void unSetAssignee(String memeber);

    String getAssignee();
}
