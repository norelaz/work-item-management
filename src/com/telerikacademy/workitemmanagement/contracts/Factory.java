package com.telerikacademy.workitemmanagement.contracts;

public interface Factory {
    Member createMember(String name);

    Team createTeam(String name);

}
