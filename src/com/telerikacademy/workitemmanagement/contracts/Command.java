package com.telerikacademy.workitemmanagement.contracts;

import java.util.List;

public interface Command {
    String getCommandName();

    List<String> getParameters();
}
