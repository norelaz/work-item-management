package com.telerikacademy.workitemmanagement.contracts;

import java.util.List;

public interface MemberAndBoard {
    String getName();

    List<WorkItem> getWorkItems();

    void addActivity(String activity);

    String getHistory();
}
