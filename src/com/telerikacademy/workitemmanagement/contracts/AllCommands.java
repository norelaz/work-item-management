package com.telerikacademy.workitemmanagement.contracts;

import com.telerikacademy.workitemmanagement.enums.PriorityType;
import com.telerikacademy.workitemmanagement.enums.SeverityType;
import com.telerikacademy.workitemmanagement.enums.SizeType;
import com.telerikacademy.workitemmanagement.enums.StatusType;

public interface AllCommands {
    String createMember(String memberName);

    String showAllPeople();

    String showMemberActivity(String memberName);

    String createTeam(String teamName);

    String showAllTeams();

    String showTeamActivity(String teamName);

    String addMemberToTeam(String memberName, String teamName);

    String showAllTeamMembers(String teamName);

    String createABoardInTeam(String boardName, String teamName);

    String showAllTeamBoards(String teamName);

    String showBoardActivity(String boardName);

    String createNewBugInBoard(String boardName, String id, String title, String description, StatusType statusType, PriorityType priorityType, SeverityType severityType);

    String createNewStoryInBoard(String boardName, String id, String title, String description, StatusType statusType, PriorityType priorityType, SizeType sizeType);

    String createNewFeedBackInBoard(String boardName, String id, String title, String description, StatusType statusType, int rating);

    String changeWorkItemfeature(String id, String feature);

    String assignWorkItemToMember(String memberName, String id);

    String unassignWorkItemToMember(String memberName, String id);

    String addCommentToAWorkItem(String id, String comment);

    String listAll();

    String filterByInstance(String filteringArgument);

    String filterByStatus(String type);

    String filterByAssignee(String assignee);

    String filterByStatusAndAssignee(String assignee, String type);

    String listSortedBy(String sortingOption);

    StatusType getStatusType(String statusType);

    PriorityType getPriorityType(String priorityType);

    SeverityType getSeverityType(String severityType);

    SizeType getSizeType(String sizeType);

}
