package com.telerikacademy.workitemmanagement.contracts;

import com.telerikacademy.workitemmanagement.enums.PriorityType;
import com.telerikacademy.workitemmanagement.enums.SeverityType;
import com.telerikacademy.workitemmanagement.enums.SizeType;
import com.telerikacademy.workitemmanagement.enums.StatusType;

public interface Board extends MemberAndBoard{
    Bug createNewBug(String id, String title, String description, StatusType statusType,
                      PriorityType priorityType, SeverityType severityType);

    Story createNewStory(String id, String title, String description, StatusType statusType,
                        PriorityType priorityType, SizeType size);

    Feedback  createNewFeedback(String id, String title, String description, StatusType statusType, int rating);

}
