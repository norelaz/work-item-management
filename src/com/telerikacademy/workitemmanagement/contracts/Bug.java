package com.telerikacademy.workitemmanagement.contracts;

import com.telerikacademy.workitemmanagement.enums.PriorityType;
import com.telerikacademy.workitemmanagement.enums.SeverityType;
import java.util.List;

public interface Bug extends Assignable {
    List<String> getSteps();

    SeverityType getSeverityType();

    void setPriorityType(PriorityType priorityType);

    void setSeverityType(SeverityType severityType);
}
